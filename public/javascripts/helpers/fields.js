define(function () {
  var Fields = function () {
    var COLS = 'abcdefgh',
        ROWS = [1, 2, 3, 4, 5, 6, 7, 8];

    return function (f1, f2) {
      var i1X       = COLS.indexOf(f1[0]),
          i2X       = COLS.indexOf(f2[0]),
          i1Y       = ROWS.indexOf(+f1[1]),
          i2Y       = ROWS.indexOf(+f2[1]),
          distanceX = Math.abs(i1X - i2X),
          distanceY = Math.abs(i1Y - i2Y);

      var color = function (x, y) {
        return ((x + y) % 2 === 0) ? 'w' : 'b';
      };

      return {
        field1: f1,
        field2: f2,
        distanceX: distanceX,
        distanceY: distanceY,
        distance: Math.max(distanceY, distanceX),
        color1: color(i1X, i1Y),
        color2: color(i2X, i2Y),
        north: i1Y < i2Y && i1X == i2X,
        south: i1Y > i2Y && i1X == i2X,

        crossedFields: (function () {
          var x      = f1[0],
              y      = +f1[1],
              yMax   = +f2[1],
              fields = [];

          while (x != f2[0] || y != yMax) {
            fields.push(x + y);

            if (x < f2[0]) {
              x = String.fromCharCode(x.charCodeAt() + 1);
            } else if (x > f2[0]) {
              x = String.fromCharCode(x.charCodeAt() - 1);
            }

            if (y < yMax) {
              y += 1;
            } else if (y > yMax) {
              y -= 1;
            }
          }

          return fields.splice(1);
        })()
      };
    };
  }();

  return Fields;
});
