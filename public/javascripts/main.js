require.config({
  paths: {
    jquery: 'libs/jquery-1.7.1.min',
    underscore: 'libs/underscore-min',
    backbone: 'libs/backbone-min'
  },

  baseUrl: '/javascripts'
});

require(
  ['app', 'models/chess', 'views/chess', 'views/undo', 'views/redo'],
  function (App, Chess, ChessView, Undo, Redo) {
    App.init(new Chess());

    new Undo;
    new Redo;
  }
);
