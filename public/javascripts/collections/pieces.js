define(
  ['backbone', 'models/chessman', 'views/chessman'],
  function (Backbone, Chessman, ChessmanView) {
    var Pieces = Backbone.Collection.extend({
      model: Chessman,

      initialize: function () {
        this.on('add', function (piece) {
          var view = new ChessmanView({
            model: piece,
            className: piece.get('type') + ' chessman'
          });

          piece.view = view;
        });

        this.on('remove', function (piece) {
          piece.view.remove();
        });
      },

      onFields: function (fields) {
        return this.filter(function(piece) {
          return fields.indexOf(piece.get('position')) !== -1;
        });
      },

      removeAll: function () {
        // Workaround:
        // According to https://github.com/documentcloud/backbone/issues/263 you can't delete all model objects from a collection at once.
        var self = this;

        _(this.map(function(piece) {
          return piece.cid;
        })).each(function(cid) {
          self.remove(self.getByCid(cid));
        });
      }
    });

    return Pieces;
  }
);
