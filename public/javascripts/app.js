define(
  ['views/chess'],
  function (ChessView) {
    var App = {
      init: function (game) {
        this.game = game;

        game.view = new ChessView({
          el: $('#chessboard'),
          model: this.game
        });

        this.socket = io.connect();
        this.socket.on('newState', function (data) {
          game.loadPositions(data.state.b, data.state.w);
          game.toggleColor();
        });
      }
    };

    return App;
  }
);
