define(
  ['backbone', 'app'],
  function (Backbone, App) {
    var ChessmanView = Backbone.View.extend({
      events: {
        'click': 'activate'
      },

      initialize: function () {
        this.$el.addClass(this.model.get('color'));

        this.render();
      },

      render: function () {
        $('#' + this.model.get('position')).append(this.$el);
      },

      activate: function () {
        if (App.game.get('activeColor') === this.model.get('color')) {
          this.model.set('active', true);
        }
      }
    });

    return ChessmanView;
  }
);
