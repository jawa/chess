define(
  ['backbone', 'app'],
  function (Backbone, App) {
    var Undo = Backbone.View.extend({
      el: '#undo',

      events: {
        'click': 'undo'
      },

      undo: function (e) {
        App.game.undo();

        e.preventDefault();
      }
    });

    return Undo;
  }
);
