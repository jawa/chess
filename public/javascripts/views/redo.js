define(
  ['backbone', 'app'],
  function (Backbone, App) {
    var Redo = Backbone.View.extend({
      el: '#redo',

      events: {
        'click': 'redo'
      },

      redo: function (e) {
        App.game.redo();

        e.preventDefault();
      }
    });

    return Redo;
  }
);
