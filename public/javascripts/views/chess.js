define(
  ['backbone', 'helpers/fields', 'text!templates/chessboard.html'],
  function (Backbone, Fields, chessboard) {
    var ChessView = Backbone.View.extend({
      events: {
        'click .field': 'movePiece'
      },

      initialize: function () {
        this.render('w');
      },

      render: function (color) {
        var rows  = [1, 2, 3, 4, 5, 6, 7, 8],
            cols  = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

        if (color === 'w') {
          rows = rows.reverse();
          cols = cols.reverse();
        }

        this.$el.html(_.template(chessboard, { rows: rows, cols: cols }));
        this.model.reload();
      },

      movePiece: function (e) {
        var $field    = $(e.currentTarget),
            field     = $field.attr('id'),
            piece     = this.model.get('activePiece'),
            curPos    = piece && piece.get('position'),
            pieces    = this.model.get('pieces'),
            isBlocked = $field.find('.chessman').length > 0;

        if (!piece || field === curPos) {
          return false;
        }

        if (isBlocked && piece.validateTake(field)) {
          pieces.remove(pieces.find(function (piece) {
            return piece.get('position') === field;
          }));
        }
        else if (isBlocked || !piece.validateMove(Fields(piece.get('position'), field))) {
          return false;
        }

        piece.set('position', field);
        piece.set('active', false);
        this.model.toggleColor();
      }
    });

    return ChessView;
  }
);
