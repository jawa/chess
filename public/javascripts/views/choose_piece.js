define(
  ['backbone', 'app', 'models/queen', 'models/rook', 'models/bishop', 'models/knight'],
  function (Backbone, App, Queen, Rook, Bishop, Knight) {
    var ChoosePiece = Backbone.View.extend({
      el: '#choose-piece',

      initialize: function (pawn) {
        this.pawn = pawn;

        this.$el.show();
      },

      events: {
        'click .pieces *': 'showPiece'
      },

      showPiece: function (e) {
        var color = this.pawn.get('color'),
            field = this.pawn.get('position');

        switch($(e.currentTarget).attr('class')) {
          case 'queen':
            var piece = new Queen({ color: color, position: field });
            break;

          case 'rook':
            var piece = new Rook({ color: color, position: field });
            break;

          case 'bishop':
            var piece = new Bishop({ color: color, position: field });
            break;

          case 'knight':
            var piece = new Knight({ color: color, position: field });
            break;
        }

        App.game.get('pieces').add(piece).remove(this.pawn);

        this.$el.unbind().hide(); // unbind() will prevent the event from firing more than once.
      }
    });

    return ChoosePiece;
  }
);
