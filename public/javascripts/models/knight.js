define(
  ['models/chessman'],
  function (Chessman) {
    var Knight = Chessman.extend({
      defaults: { type: 'knight' },

      validateMove: function (move) {
        return move.distance == 2 && move.color1 !== move.color2;
      }
    });

    return Knight;
  }
);
