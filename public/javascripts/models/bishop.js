define(
  ['models/chessman', 'app'],
  function (Chessman, App) {
    var Bishop = Chessman.extend({
      defaults: { type: 'bishop' },

      validateMove: function (move) {
        return App.game.get('pieces').onFields(move.crossedFields).length === 0 && (move.distanceX === move.distanceY);
      }
    });

    return Bishop;
  }
);
