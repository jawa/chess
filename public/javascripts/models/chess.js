define(
  ['app', 'collections/pieces', 'models/king', 'models/queen', 'models/rook', 'models/bishop', 'models/knight', 'models/pawn'],
  function (App, Pieces, King, Queen, Rook, Bishop, Knight, Pawn) {
    var Chess = Backbone.Model.extend({
      defaults: {
        activeColor: 'w',
        history: [],
        futureHistory: [],
        kingsidew: true,
        kingsideb: true,
        queensidew: true,
        queensideb: true
      },

      initialize: function () {
        this.loadPositions({
          a8: 'r',
          b8: 'kn',
          c8: 'b',
          d8: 'k',
          e8: 'q',
          f8: 'b',
          g8: 'kn',
          h8: 'r',

          a7: 'p',
          b7: 'p',
          c7: 'p',
          d7: 'p',
          e7: 'p',
          f7: 'p',
          g7: 'p',
          h7: 'p'
        }, {
          a1: 'r',
          b1: 'kn',
          c1: 'b',
          d1: 'k',
          e1: 'q',
          f1: 'b',
          g1: 'kn',
          h1: 'r',

          a2: 'p',
          b2: 'p',
          c2: 'p',
          d2: 'p',
          e2: 'p',
          f2: 'p',
          g2: 'p',
          h2: 'p'
        });

        this.savePositions(true);
      },

      toggleColor: function () {
        var color = this.get('activeColor');

        this.set('activeColor', color === 'w' ? 'b' : 'w');
      },

      loadPositions: function (black, white) {
        var add = function (color) {
          return function (piece, field) {
            switch (piece) {
              case 'k':
                return new King({ 'color': color, 'position': field });
                break;

              case 'q':
                return new Queen({ 'color': color, 'position': field });
                break;

              case 'r':
                return new Rook({ 'color': color, 'position': field });
                break;

              case 'b':
                return new Bishop({ 'color': color, 'position': field });
                break;

              case 'kn':
                return new Knight({ 'color': color, 'position': field });
                break;

              case 'p':
                return new Pawn({ 'color': color, 'position': field });
                break;
            }
          };
        };

        var pieces = this.get('pieces') || new Pieces();

        pieces.removeAll();

        pieces.add(_(black).map(add('b')).concat(_(white).map(add('w'))));

        this.set('pieces', pieces);
      },

      savePositions: function (init) {
        var state    = {b: {}, w: {}},
            previous = this.get('state'),
            history  = this.get('history');

        this.get('pieces').each(function(piece) {
          var type = piece.get('type');

          state[piece.get('color')][piece.get('position')] = type === 'knight' ? 'kn' : type[0];
        });

        if (history.length >= 10) {
          history.shift();
        }

        if (previous) {
          history.push(previous);
        }

        this.set('state', state);

        if (!init) {
          App.socket.emit('move', { state: state });
        }
      },

      undo: function () {
        var state   = this.get('history').pop(),
            current = this.get('state'),
            future  = this.get('futureHistory');

        this.loadPositions(state['b'], state['w']);
        this.toggleColor();

        future.push(current);
        this.set('state', state);
      },

      redo: function () {
        var state   = this.get('futureHistory').pop(),
            current = this.get('state'),
            history = this.get('history');

        this.loadPositions(state['b'], state['w']);
        this.toggleColor();

        history.push(current);
        this.set('state', state);
      },

      reload: function () {
        var current = this.get('state');

        this.loadPositions(current.b, current.w);
      }
    });

    return Chess;
  }
);
