define(
  ['models/chessman', 'app'],
  function (Chessman, App) {
    var Queen = Chessman.extend({
      defaults: { type: 'queen' },

      validateMove: function (move) {
        return App.game.get('pieces').onFields(move.crossedFields).length === 0 && (move.distanceX === 0 || move.distanceY === 0 || move.distanceX === move.distanceY);
      }
    });

    return Queen;
  }
);
