define(
  ['models/chessman', 'app'],
  function (Chessman, App) {
    var Rook = Chessman.extend({
      defaults: { type: 'rook' },

      validateMove: function (move) {
        var color = this.get('color');

        if (App.game.get('pieces').onFields(move.crossedFields).length === 0 && (move.distanceX === 0 || move.distanceY === 0)) {
          if (move.field1 === 'a' + (color === 'w' ? 1 : 8)) {
            App.game.set('kingside' + color, false);
          } else if (move.field1 === 'h' + (color === 'w' ? 1 : 8)) {
            App.game.set('queenside' + color, false);
          }

          return true;
        }
      }
    });

    return Rook;
  }
);
