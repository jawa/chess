define(
  ['models/chessman', 'app', 'views/choose_piece'],
  function (Chessman, App, ChoosePiece) {
    var Pawn = Chessman.extend({
      defaults: { type: 'pawn' },

      validateMove: function (move) {
        var w         = this.get('color') === 'w',
            start     = move.distance === 2 && this.get('position')[1] === (w ? '2' : '7'),
            direction = w ? move.north : move.south;

        if (App.game.get('pieces').onFields(move.crossedFields).length !== 0) {
          return false;
        }
        else if (direction && (move.distance === 1 || start)) {
          if (move.field2[1] === '1' || move.field2[1] === '8') {
            new ChoosePiece(this);
          }

          return true;
        }
      },

      validateTake: function (field) {
        var position    = this.get('position'),
            leftOrRight = position[0] === String.fromCharCode(field[0].charCodeAt() + 1)
                       || position[0] === String.fromCharCode(field[0].charCodeAt() - 1);

        if (this.get('color') === 'w') {
          return +position[1] + 1 === +field[1] && leftOrRight;
        } else {
          return +position[1] - 1 === +field[1] && leftOrRight;
        }
      }
    });

    return Pawn;
  }
);
