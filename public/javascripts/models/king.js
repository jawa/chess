define(
  ['models/chessman', 'helpers/fields', 'app'],
  function (Chessman, Field, App) {
    var King = Chessman.extend({
      defaults: { type: 'king' },

      validateMove: function (move) {
        var normalMove      = move.distance === 1,
            color           = this.get('color'),
            row             = color === 'w' ? 1 : 8,
            castleKingside  = App.game.get('kingside' + color) && move.field2[0] === 'b' && App.game.get('pieces').onFields(['b' + row, 'c' + row]).length === 0,
            castleQueenside = App.game.get('queenside' + color) && move.field2[0] === 'f' && App.game.get('pieces').onFields(['e' + row, 'f' + row, 'g' + row]).length === 0;

        if (!normalMove && !castleKingside && !castleQueenside) {
          return false;
        }

        if (castleKingside || castleQueenside) {
          var rook = App.game.get('pieces').where({ position: (castleKingside ? 'a' : 'h') + row })[0];

          if (rook) rook.set('position', (castleKingside ? 'c' : 'e') + row);
        }

        App.game.set('kingside' + color, false);
        App.game.set('queenside' + color, false);

        return true;
      }
    });

    return King;
  }
);
