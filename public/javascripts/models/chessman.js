define(
  ['backbone', 'helpers/fields', 'app'],
  function (Backbone, Fields, App) {
    var Chessman = Backbone.Model.extend({
      defaults: { active: false },

      initialize: function () {
        this.on('change:position', function () {
          this.view.render();

          App.game.savePositions();
        });

        this.on('change:active', function () {
          var activePiece = App.game.get('activePiece');

          if (this.get('active')) {
            if (activePiece) activePiece.set('active', false);

            App.game.set('activePiece', this);
            this.view.$el.addClass('active');
          } else {
            this.view.$el.removeClass('active');
          }
        });
      },

      validateTake: function (field) {
        return this.validateMove(Fields(this.get('position'), field));
      }
    });

    return Chessman;
  }
);
