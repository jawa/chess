
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    app = module.exports = express.createServer(),
    io = require('socket.io').listen(app);

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);

app.get('/game/:id', function (req, res) {
  var gameID = req.route.params['id'];
  // TODO: Look for gameID in the database and suggest to create a new game if it doesn't exist yet

  res.render('index', {
    title: 'Chess'
  });
});

io.sockets.on('connection', function (socket) {
  socket.join(socket.handshake.headers.referer);

  socket.on('move', function (data) {
    socket.broadcast.to(socket.handshake.headers.referer).emit('newState', data);
  });
});

app.listen(process.env.PORT || 3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
